﻿Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modMain
   Public dbConn As ADODB.Connection
   Public dbRs As ADODB.Recordset
   Public dbCmd As ADODB.Command

   Public g_sProvider As String
   Public g_sFieldDef As String
   Public g_sCityPath As String
   Public g_sInputPath As String
   Public g_sSfxTable As String
   Public g_sLkupTable As String
   Public g_sLkupTmpl As String
   Public g_sCityTbl As String
   Public g_sTblTmpl As String
   Public g_sCnty As String
   Public g_sErrMsg As String
   Public g_lstCnty() As String
   Public g_iCnty As Integer

   Sub Main()
      Dim sCmd As String
      Dim iTmp As Integer, iRet As Integer

      sCmd = Command()
      If sCmd <= " " Then
         Console.WriteLine("Usage: LoadCity <cntycode,cntycode, ...>")
         Exit Sub
      End If

      InitSettings(sCmd)

      If sCmd.IndexOf(Asc(",")) > 0 Then
         g_iCnty = ParseStr(sCmd, ",", g_lstCnty)
         For iTmp = 0 To g_iCnty - 1
            iRet = loadCityTbl(g_lstCnty(iTmp))
            LogMsg(g_lstCnty(iTmp) & ": " & iRet)
         Next
      Else
         iRet = loadCityTbl(sCmd)
         LogMsg(sCmd & ": " & iRet)
      End If
   End Sub

   'Return number of cities if loading successful
   Function loadCityTbl(ByRef sCnty As String) As Integer
      Dim fh, iCnt, iTmp As Integer
      Dim sTmp, sErrMsg, sCityFile As String

      sCityFile = g_sCityPath & "\" & sCnty & "City.dat"
      fh = FreeFile()
      Try
         FileOpen(fh, sCityFile, OpenMode.Input, OpenAccess.Read, OpenShare.Shared)
         sTmp = LineInput(fh)
         iCnt = sTmp
      Catch ex As ApplicationException
         sErrMsg = ex.Message
         GoTo loadCityTbl_Err
      End Try

      'Get connection
      Try
         LogMsg("Open db connection: " & g_sProvider)
         dbConn = New ADODB.Connection
         dbConn.ConnectionString = "Provider=" & g_sProvider
         dbConn.Open()
      Catch ex As Exception
         LogMsg("***** Cannot open DB. Provider=" & g_sProvider)
         sErrMsg = ex.Message
         GoTo loadCityTbl_Err
      End Try

      'Remove old city
      LogMsg("Truncate old data from: " & g_sCityTbl)
      Try
         dbCmd = New ADODB.Command
         dbCmd.ActiveConnection = dbConn
         dbCmd.CommandText = "DELETE FROM " & g_sCityTbl & " WHERE CountyCode='" & sCnty & "'"
         dbCmd.Execute()
      Catch ex As Exception
         LogMsg("***** Cannot delete old data from " & g_sCityTbl)
         sErrMsg = ex.Message
         GoTo loadCityTbl_Err
      End Try

      'Open table to load new city
      LogMsg("Open table " & g_sCityTbl)
      Try
         dbRs = New ADODB.Recordset
         dbRs.CursorType = ADODB.CursorTypeEnum.adOpenKeyset
         dbRs.LockType = ADODB.LockTypeEnum.adLockOptimistic
         dbRs.Open(g_sCityTbl, dbConn, , , ADODB.CommandTypeEnum.adCmdTable)
         LogMsg("Loop through " & sCityFile)

         'Drop blank line
         sTmp = LineInput(fh)
         For iTmp = 1 To iCnt - 1
            sTmp = LineInput(fh)
            If Left(sTmp, 1) > " " Then
               dbRs.AddNew()
               dbRs("CountyCode").Value = sCnty
               dbRs("CityCode").Value = ""
               dbRs("CityIdx").Value = iTmp
               dbRs("CityName").Value = sTmp
               dbRs.Update()
            End If
         Next
         dbRs.Close()
      Catch ex As Exception
         LogMsg("***** Error updating " & g_sCityTbl)
         sErrMsg = ex.Message
         GoTo loadCityTbl_Err
      End Try

      FileClose(fh)
      loadCityTbl = iTmp
      Exit Function
loadCityTbl_Err:
      If sErrMsg > " " Then
         MsgBox("Error: " & sErrMsg)
         LogMsg(sErrMsg)
      End If
      loadCityTbl = 0
   End Function

   Private Function InitSettings(ByRef sCmd As String) As Boolean
      Dim sTmp As String

      sTmp = My.Settings.LogDir
      g_logFile = sTmp & "\" & My.Application.Info.AssemblyName & ".log"
      g_sCityPath = My.Settings.CityDir
      g_sCityTbl = My.Settings.CityTbl
      g_sProvider = My.Settings.dbProvider

      If sCmd = "ALL" Then
         sCmd = My.Settings.AllCnty
      ElseIf sCmd = "WORK" Then
         sCmd = My.Settings.WorkCnty
      End If

      InitSettings = True
      If g_sProvider = "" Then
         LogMsg("DB provider has not been defined. Please check config file")
         InitSettings = False
      End If
      If g_sCityPath = "" Then
         LogMsg("Directory of city files has not been defined. Please check config file")
         InitSettings = False
      End If
      If g_sCityTbl = "" Then
         LogMsg("No City table defined. Use default (tblCity)")
         g_sCityTbl = "tblCity"
      End If
   End Function

End Module
