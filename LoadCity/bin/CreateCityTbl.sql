USE [Spatial]
GO
/****** Object:  Table [dbo].[CityTbl]    Script Date: 10/26/2009 01:13:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CityTbl](
   [CountyCode] [char](3) NOT NULL,
   [CityIdx] [smallint] NOT NULL,
   [CityName] [varchar](50) NOT NULL,
   [CityCode] [varchar](20) NULL,
 CONSTRAINT [PK_CityTbl] PRIMARY KEY CLUSTERED
(
   [CountyCode] ASC,
   [CityIdx] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF